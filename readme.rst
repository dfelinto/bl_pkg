
##################
Blender Extensions
##################

Directory Layout
================

``./blender_addon/bl_pkg/cli/``
   The stand-alone command line utility to manage extensions.

``./blender_addon/bl_pkg/``
   The Blender add-on which wraps the command line utility
   (abstracts details of interacting with the package manager & repositories).

``./tests/``
   Automated tests.

   To run tests via the ``Makefile``.

   Test the command line application.

   .. code-block::

      make test PYTHON_BIN=/path/to/bin/python3.11

   If your system Python is v3.11 or newer you may omit ``PYTHON_BIN``.

   .. code-block::

      make test_blender BLENDER_BIN=/path/to/blender


GUI
===

This GUI is work-in-progress, currently it's been made to work with an un-modified Blender 4.0.

- Link ``blender_addon/bl_pkg`` into your add-ons directly.
- Enable the blender extensions add-on from Blender.
- Enable the blender extensions checkbox in the add-ons preference (this is a temporary location).
- Repositories can be added/removed from the "Files" section in the preferences.


Hacking
=======

Some useful hints.

When developing the command line interface, these tests can be setup to run on file-change, run:

.. code-block::

   make watch_test

To run Blender tests.

.. code-block::

   make watch_test_blender BLENDER_BIN=/path/to/blender
