# SPDX-License-Identifier: GPL-2.0-or-later

bl_info = {
    "name": "Example",
    "author": "ideasman42, phymec, Sergey Sharybin",
    "version": (0, 2),
    "blender": (2, 80, 0),
    "location": "Viewport Object Menu -> Quick Effects",
    "description": "Fractured Object Creation",
    "warning": "",
    "doc_url": "{BLENDER_MANUAL_URL}/addons/object/cell_fracture.html",
    "category": "Object",
}

pass
