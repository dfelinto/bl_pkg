# SPDX-FileCopyrightText: 2023 Blender Foundation
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
GUI (WARNING) this is a hack!
Written to allow a UI without modifying Blender.
"""

__all__ = (
    "register",
    "unregister",
)

import bpy

from bpy.types import (
    Panel,
)

from bl_ui.space_userpref import (
    USERPREF_PT_extensions,
    ExtensionsPanel,
)

from . import repo_status_text

# -----------------------------------------------------------------------------
#

def size_as_fmt_string(num: float) -> str:
    for unit in ("b", "kb", "mb", "gb", "tb", "pb", "eb", "zb"):
        if abs(num) < 1024.0:
            return "{:3.1f}{:s}".format(num, unit)
        num /= 1024.0
    unit = "yb"
    return "{:.1f}{:s}".format(num, unit)


def sizes_as_percentage_string(size_partial: int, size_final: int) -> str:
    if size_final == 0:
        percent = 0.0
    else:
        size_partial = min(size_partial, size_final)
        percent = size_partial / size_final

    return "{:-6.2f}%".format(percent * 100)


def userpref_addons_draw_ext(self, _context, search_lower, installed_only):
    """
    Show all the items... we may want to paginate at some point.
    """
    from .bl_extension_ops import (
        blender_extension_mark,
        blender_extension_show,
        extension_repos_read,
        pkg_info_check_exclude_filter,
        repo_cache_store_refresh_from_prefs,
    )

    from . import repo_cache_store

    # This isn't elegant, but the preferences aren't available on registration.
    if not repo_cache_store.is_init():
        repo_cache_store_refresh_from_prefs()

    layout = self.layout

    # Define a top-most column to place warnings (if-any).
    # Needed so the warnings aren't mixed in with other content.
    layout_topmost = layout.column()

    repos_all = extension_repos_read()

    for repo_index, (
            pkg_manifest_remote,
            pkg_manifest_local,
    ) in enumerate(zip(
        repo_cache_store.pkg_manifest_from_remote_ensure(),
        repo_cache_store.pkg_manifest_from_local_ensure(),
    )):
        if pkg_manifest_remote is None:
            box = layout_topmost.box()
            box.label(text="Repo: {!r} unable to read!".format(repos_all[repo_index].directory))
            continue

        for pkg_id, item_remote in pkg_manifest_remote.items():
            if search_lower and not pkg_info_check_exclude_filter(item_remote, search_lower):
                continue

            item_version = item_remote["version"]

            item_local = pkg_manifest_local.get(pkg_id)
            is_installed = item_local is not None

            if installed_only and (is_installed == 0):
                continue

            if item_local is None:
                item_local_version = None
                is_outdated = False
            else:
                item_local_version = item_local["version"]
                is_outdated = item_local_version != item_version

            key = (pkg_id, repo_index)
            mark = key in blender_extension_mark
            show = key in blender_extension_show
            del key

            box = layout.box()

            # Left align so the operator text isn't centered.
            row = box.row(align=True)
            # row.label
            if show:
                props = row.operator("bl_pkg.pkg_show_clear", text="", icon='DISCLOSURE_TRI_DOWN', emboss=False)
            else:
                props = row.operator("bl_pkg.pkg_show_set", text="", icon='DISCLOSURE_TRI_RIGHT', emboss=False)
            props.pkg_id = pkg_id
            props.repo_index = repo_index
            del props

            row_button = row.row()
            row_button.alignment = 'LEFT'

            label = item_remote["name"]
            if is_outdated:
                label = label + " (outdated)"

            if mark:
                props = row_button.operator("bl_pkg.pkg_mark_clear", text=label, icon='CHECKBOX_HLT', emboss=False)
            else:
                props = row_button.operator("bl_pkg.pkg_mark_set", text=label, icon='CHECKBOX_DEHLT', emboss=False)
            props.pkg_id = pkg_id
            props.repo_index = repo_index
            del props

            row_button.active = mark
            del row_button

            row_right = row.row()
            row_right.alignment = 'RIGHT'

            # pylint: disable-next=using-constant-test
            if False:
                if is_installed:
                    props = row_right.operator("bl_pkg.pkg_uninstall", text="UnInstall")
                else:
                    props = row_right.operator("bl_pkg.pkg_install", text="Install")
                props.directory = repos_all[repo_index].directory
                props.pkg_id = pkg_id
            else:
                row_right.label(text="", icon='PACKAGE' if is_installed else 'DOT')

            if show:
                split = box.split(factor=0.15)
                row_a = split.column()
                row_b = split.column()

                row_a.label(text="Version:")
                if item_local_version is not None and (item_version != item_local_version):
                    row_b.label(text="{:s} ({:s} available)".format(item_local_version, item_version))
                else:
                    row_b.label(text=item_version)

                row_a.label(text="Description:")
                row_b.label(text=item_remote["description"])

                row_a.label(text="Repo:")
                row_b.label(text=repos_all[repo_index].repo_url)

                row_a.label(text="Size:")
                row_b.label(text=size_as_fmt_string(item_remote["archive_size"]))


class USERPREF_PT_extensions_bl_pkg(ExtensionsPanel, Panel):
    bl_label = "Extensions"
    # Keep header so panel can be moved under the repositories list until design is finished.
    # bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        wm = context.window_manager
        layout = self.layout

        row = layout.row()
        rowsub = row.row(align=True)
        rowsub.operator("bl_pkg.pkg_install_marked", icon='IMPORT', text="Install")
        rowsub.operator("bl_pkg.pkg_uninstall_marked", icon='X', text="Uninstall")
        rowsub.operator("bl_pkg.repo_sync_all", text="Refresh", icon='FILE_REFRESH')
        rowsub.operator("bl_pkg.pkg_upgrade_all", text="Upgrade", icon='FILE_REFRESH')

        row.menu("BL_EXT_MT_extra", text="", icon='COLLAPSEMENU')

        split = layout.split(factor=0.5)
        split.prop(wm, "extension_installed_only")
        split.prop(wm, "extension_search", text="", icon='VIEWZOOM')

        if repo_status_text.log:
            box = layout.box()
            row = box.split(factor=0.5, align=True)
            if repo_status_text.running:
                row.label(text=repo_status_text.title + "...")
            else:
                row.label(text=repo_status_text.title)
            rowsub = row.row(align=True)
            rowsub.alignment = 'RIGHT'
            rowsub.operator("bl_pkg.pkg_status_clear", text="", icon='X', emboss=False)
            boxsub = box.box()
            for ty, msg in repo_status_text.log:
                if ty == 'STATUS':
                    boxsub.label(text=msg)
                elif ty == 'PROGRESS':
                    msg_str, progress_unit, progress, progress_range = msg
                    if progress <= progress_range:
                        boxsub.progress(
                            factor=progress / progress_range,
                            text="{:s}, {:s}".format(
                                sizes_as_percentage_string(progress, progress_range),
                                msg_str,
                            ),
                        )
                    elif progress_unit == 'BYTE':
                        boxsub.progress(factor=0.0, text="{:s}, {:s}".format(msg_str, size_as_fmt_string(progress)))
                    else:
                        # We might want to support other types.
                        boxsub.progress(factor=0.0, text="{:s}, {:d}".format(msg_str, progress))
                else:
                    boxsub.label(text="{:s}: {:s}".format(ty, msg))

            # Hide when running.
            if repo_status_text.running:
                return

        userpref_addons_draw_ext(
            self,
            context,
            wm.extension_search.lower(),
            wm.extension_installed_only,
        )


def register():
    USERPREF_PT_extensions.unused = False
    bpy.utils.register_class(USERPREF_PT_extensions_bl_pkg)


def unregister():
    USERPREF_PT_extensions.unused = True
    bpy.utils.unregister_class(USERPREF_PT_extensions_bl_pkg)
